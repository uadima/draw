package ua.dima.test;


public class Main {
    public static void main(String[] args) {
        Sequences sequences = new Sequences(10101);
        sequences.combine();
        sequences.draw();
    }
}

class Sequences {
    String sequence;
    int length;
    int iterator = 0;
    int x[][];

    Sequences(int sequence) {
        this.sequence = Integer.toString(sequence);
        this.length= this.sequence.length() * 5;
        this.x= new int[5][length];
    }

    void combine() {
        for (char c : sequence.toCharArray()
                ) {
            fill(c);
            iterator++;

        }
    }

     void draw() {
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < length; j++) {
                System.out.print(x[i][j] == 0 ? " " : x[i][j]-1);
            }
            System.out.println();
        }
    }


    void fill(char c) {
        switch (c) {
            case '0':
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++) {
                        x[i][j + (iterator * 5)] = Zero.get()[i][j];
                    }
                }
                break;
            case '1':
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++) {
                        x[i][j + (iterator * 5)] = One.get()[i][j];
                    }
                }
                break;
            case '2':
                break;
            case '3':
                break;
            case '4':
                break;
            case '5':
                break;
            case '6':
                break;
            case '7':
                break;
            case '8':
                break;
            case '9':
                break;
        }
    }
}

class Zero {
    static int x[][] = new int[5][5];

    static int[][] get() {
        x[0][2] = x[4][2] = 1;
        x[1][1] = x[3][1] = 1;
        x[3][3] = x[1][3] = 1;
        x[2][0] = x[2][4] = 1;
        return x;
    }
}

class One {
    static int x[][] = new int[5][5];

    static int[][] get() {
        x[0][2] = 2;
        x[1][2] = 2;
        x[2][2] = 2;
        x[3][2] = 2;
        x[4][2] = 2;
        x[1][1] = 2;
        x[2][0] = 2;
        x[4][1] = 2;
        x[4][3] = 2;
        return x;
    }
}
